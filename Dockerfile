ARG node_version
FROM node:${node_version}-alpine

# Create working directory
WORKDIR /usr/src/app

# Install app dependencies
COPY package*.json ./

RUN npm install

# Bundle app source
COPY . .

# Start application at port 8080
EXPOSE 8080
CMD ["npm", "start"]
