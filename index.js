'use strict';

const express = require('express');

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// Application
const app = express();
app.get('/', (request, response) => {
    response.redirect('/api');
});
app.get('/api', (request, response) => {
    let data = {
        status: "success",
	message: "Hello World!\n"
    };

    response.send(data);
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
